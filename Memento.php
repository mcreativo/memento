<?php


class Memento
{

    /**
     * @var array
     */
    protected $state;

    /**
     * @param array $state
     * @return Memento
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return array
     */
    public function getState()
    {
        return $this->state;
    }


} 