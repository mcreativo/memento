<?php


class ClassA
{
    /**
     * @var string
     */
    protected $attribute;

    /**
     * @param  string $attribute
     */
    function __construct($attribute)
    {
        $this->attribute = $attribute;
    }

    public function doSomething()
    {
        echo 'doing something with attribute: ' . $this->attribute . PHP_EOL;
    }

    /**
     * @param string $attribute
     * @return ClassA
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    public function createMemento()
    {
        return (new Memento())->setState(['attribute' => $this->attribute]);
    }

    public function setMemento(Memento $m)
    {
        $state = $m->getState();
        $this->attribute = $state['attribute'];
    }


} 